package com.example.storagedemoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startInternalStorageDemoActivity(View view) {
        Intent intent = new Intent(this,InternalStorageDemoActivity.class);
        startActivity(intent);
    }

    public void startExternalStorageDemoActivity(View view) {
        Intent intent = new Intent(this,ExternalStorageDemoActivity.class);
        startActivity(intent);
    }
}
